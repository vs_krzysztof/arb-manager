// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
declare global {
	namespace App {
		// interface Error {}
		// interface Locals {}
		// interface PageData {}
		// interface PageState {}
		// interface Platform {}
	}

	type Metadata = Record<string, string>;
	type Messages = Record<string, { message: string; metadata: Record<string, unknown> }>;
	type ParsedArbFile = {
		filename: string;
		metadata: Metadata;
		messages: Messages;
		messages_keys: string[];
	};
}

export {};
