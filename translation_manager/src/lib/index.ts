export async function parse_arb_file(file: File): Promise<ParsedArbFile> {
	const text = await file.text();
	const json = JSON.parse(text);
	console.log('🚀 ~ parse_arb_file ~ json:', json);

	const keys = Object.keys(json);

	const metadata: Metadata = {};

	keys
		.filter((key) => key.startsWith('@@'))
		.forEach((key) => {
			metadata[key] = json[key];
			delete json[key];
		});

	const messages: Messages = {};

	keys
		.filter((key) => !key.startsWith('@'))
		.forEach((key) => {
			messages[key] = {
				message: json[key],
				metadata: json[`@${key}`]
			};
		});

	return {
		filename: file.name,
		metadata,
		messages,
		messages_keys: keys.filter((key) => !key.startsWith('@')).toSorted()
	};
}

export async function generate_arb_file({
	metadata,
	messages,
	filename
}: ParsedArbFile): Promise<File> {
	const json: Record<string, unknown> = {
		...metadata,
		'@@last_modified': new Date().toISOString()
	};

	const sorted_keys = Object.keys(messages).toSorted();

	for (const key of sorted_keys) {
		json[key] = messages[key].message;
		json[`@${key}`] = messages[key].metadata;
	}

	const text = JSON.stringify(json, null, '\t');

	return new File([text], filename, { type: 'application/json' });
}

export function download_file(file: File) {
	const url = URL.createObjectURL(file);
	const a = document.createElement('a');
	a.href = url;
	a.download = file.name;
	a.click();
}
